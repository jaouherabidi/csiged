<?php
	class queries extends CI_Model{
		



		/* LISTINGS  requête génération excel*/
		public function writeExcel($limit=0, $offset=0)
		{
			$this->db->select('*');
			$this->db->from('produit');
			$this->db->join('adresse', 'produit.id_produit = adresse.id_produit');
			$this->db->join('mouv_adresse', 'adresse.id_adresse = mouv_adresse.id_adresse');
			$this->db->order_by('ref_adresse', 'asc');
			$this->db->limit($limit, $offset);
			$query = $this->db->get('');
			if($query->num_rows() > 0){
				return $query->result();
			}
		}

			/* Exemple FOURNISSEUR */

		public function getFournisseurs($limit=0, $offset=0){
			$this->db->select('*');
			$this->db->from('fournisseur');
			$this->db->limit($limit, $offset);
			$query = $this->db->get('');
			if($query->num_rows() > 0){
				return $query->result();
			}			
		}

		public function addFournisseur($data){
			return $this->db->insert('fournisseur', $data);
		}

		public function getSingleFournisseur($id){
			$query = $this->db->get_where('fournisseur', array('id_fournisseur' => $id));
				if($query->num_rows() > 0){
				return $query->row();
			}
		}

		public function updateFournisseur($data, $id){
			return $this->db->where('id_fournisseur', $id)
						->update('fournisseur', $data);
		}

		public function deleteFournisseur($id){
			return $this->db->delete('fournisseur', ['id_fournisseur'=> $id]);
		}

		public function get_num_rows_fournisseur(){
			$query = $this->db->get('fournisseur');
			return $query->num_rows();
		}

		public function searchRecord_fournisseur($query){
			$this->db->select('*');
			$this->db->from('fournisseur');
			$this->db->like('nom_fournisseur', $query);
			$query = $this->db->get('');
			if($query->num_rows() > 0){
			return $query->result();
		}
	}	
}
?>