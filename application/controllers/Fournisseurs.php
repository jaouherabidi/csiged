<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fournisseurs extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		return redirect('dashboard');
	}
	
	public function rechfour()
	{
		//$this->load->model('queries');
		//$posts = $this->queries->getFournisseurs();
		//$this->load->library('pagination');
		/*$config = [
			'base_url' => base_url("fournisseurs/rechfour"),
			'per_page' => 7,
			'total_rows' => $this->queries->get_num_rows_fournisseur(),
			'uri_segment' => 3,

		];*/
		//$this->pagination->initialize($config);
		//$posts = $this->queries->getFournisseurs($config['per_page'], $this->uri->segment(3));
		//$this->load->view('fournisseurs/rechfour', ['posts' => $posts]);
                                     $this->load->view('fournisseurs/rechfour');
	}

	public function search(){
		$this->form_validation->set_rules('query', 'Query', 'required');
		$this->form_validation->set_error_delimiters('<div class"text-danger">', '</div>');
		if($this->form_validation->run()){
			$query = $this->input->post('query');
			$this->load->model('queries');
			$post = $this->queries->searchRecord_fournisseur($query);
			$this->load->view('fournisseurs/search', ['post'=>$post]);
		}
		else{
			return redirect('fournisseurs/rechfour');
		}
	}


	public function newfour()
	{
		$this->load->view('fournisseurs/newfour');
		/*$this->page = "fournisseurs/newfour";
		$this->layout();*/		
	}

	public function update($id){
		//echo $id;
		$this->load->model('queries');
		$post = $this->queries->getSingleFournisseur($id);
		$this->load->view('fournisseurs/update', ['post' => $post]);
	}

	public function save(){
		$this->form_validation->set_rules('nom_fournisseur', 'nom du fournisseur', 'required');
		$this->form_validation->set_rules('code_fournisseur', 'Code fournisseur');
		$this->form_validation->set_rules('activite', 'Activité', 'required');
		$this->form_validation->set_rules('mode_paiement', 'mode paiement');
		$this->form_validation->set_rules('adresse_fournisseur1', 'adresse fournisseur', 'required');
		$this->form_validation->set_rules('delai_paiement', 'delai paiement');
		$this->form_validation->set_rules('adresse_fournisseur2', 'complement');
		$this->form_validation->set_rules('mode_livraison', 'mode livraison');
		$this->form_validation->set_rules('ville_cp_fournisseur', 'ville et cp fournisseur');
		$this->form_validation->set_rules('delai_livraison', 'delai livraison');
		$this->form_validation->set_rules('telephone', 'téléphone');
		$this->form_validation->set_rules('fax', 'Fax');
		$this->form_validation->set_rules('mail', 'Mail');

		if($this->form_validation->run())
		{
			$data = $this->input->post();

			unset($data['submit']);
			$this->load->model('queries');
			if($this->queries->addFournisseur($data)){
				$this->session->set_flashdata('msg', 'Fournisseur ajouté avec succès');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Echec de l ajout du fournisseur');
			}
			return redirect('fournisseurs/rechfour');
		/*	echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}
		else
		{
		$this->page = "fournisseurs/newfour";
		$this->layout();
		//echo 'Il manque des informations';
		}		
	}

	public function change($id){
		$this->form_validation->set_rules('nom_fournisseur', 'nom du fournisseur', 'required');
		$this->form_validation->set_rules('code_fournisseur', 'Code fournisseur');
		$this->form_validation->set_rules('activite', 'Activité', 'required');
		$this->form_validation->set_rules('mode_paiement', 'mode paiement');
		$this->form_validation->set_rules('adresse_fournisseur1', 'adresse fournisseur', 'required');
		$this->form_validation->set_rules('delai_paiement', 'delai paiement');
		$this->form_validation->set_rules('adresse_fournisseur2', 'complement');
		$this->form_validation->set_rules('mode_livraison', 'mode livraison');
		$this->form_validation->set_rules('ville_cp_fournisseur', 'ville et cp fournisseur');
		$this->form_validation->set_rules('delai_livraison', 'delai livraison');
		$this->form_validation->set_rules('telephone', 'téléphone');
		$this->form_validation->set_rules('fax', 'Fax');
		$this->form_validation->set_rules('mail', 'Mail');

		if($this->form_validation->run())
		{
			$data = $this->input->post();

			unset($data['submit']);
			$this->load->model('queries');
			if($this->queries->updateFournisseur($data, $id)){
				$this->session->set_flashdata('msg', 'Fournisseur modifié avec succès');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Echec de la modification du fournisseur');
			}
			return redirect('fournisseurs/rechfour');
		/*	echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}
		else
		{
		$this->page = "fournisseurs/update";
		$this->layout();
		//echo 'Il manque des informations';
		}	
	}

	public function view($id){
		$this->load->model('queries');
		$post = $this->queries->getSingleFournisseur($id);
		$this->load->view('fournisseurs/view', ['post' => $post]);
	}

	public function delete($id){
		$this->load->model('queries');
		if($this->queries->deleteFournisseur($id)){
			$this->session->set_flashdata('msg', 'Fournisseur effacé avec succès');
		}
		else{
			$this->session->set_flashdata('msg', 'Echec de l effacement du fournisseur');
		}
		return redirect('fournisseurs/rechfour');
	}
}
