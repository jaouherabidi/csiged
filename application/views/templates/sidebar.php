
<div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                	<h3><i class="glyphicon glyphicon-dashboard"></i>
                    Tableau de bord</h3>
                    <strong>TB</strong>
                </div>

                <ul class="list-unstyled components">

                    <li>
                        <a   class="sidebox">
                            <i class="glyphicon glyphicon-folder-open"></i>
                            Ajouter un dossier +   
                        </a>
                    </li>
                     <li>
    
                        <a href="#articleSubmenu" data-toggle="collapse" aria-expanded="false" class="sidebox">
                            <i class="glyphicon glyphicon-folder-open"></i>
                            Dossier 1 (exemple)
                        </a>
                        <ul class="collapse list-unstyled" id="articleSubmenu">
                            <li><a href="<?php echo base_url(); ?>dashboard/" class="sideboxlist"><i class="glyphicon glyphicon-duplicate"></i>Sous-dossier 1</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/" class="sideboxlist"><i class="glyphicon glyphicon-duplicate"></i>Sous-dossier 2</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/" class="sideboxlist"><i class="glyphicon glyphicon-duplicate"></i>Sous-dossier 3</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/" class="sideboxlist"> <i class="glyphicon glyphicon-duplicate"></i>Sous-dossier 4</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#listingsSubmenu" data-toggle="collapse" aria-expanded="false" class="sidebox">
                            <i class="glyphicon glyphicon-folder-open"></i>
                            Dossier 2 (type, couleur)
                        </a>
                        <ul class="collapse list-unstyled" id="listingsSubmenu">
                            <li><a href="<?php echo base_url(); ?>dashboard/" class="sideboxlist"><i class="glyphicon glyphicon-duplicate"></i>Sous-dossier 1 (PDF)</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/" class="sideboxlist"><i class="glyphicon glyphicon-duplicate"></i>Sous-dossier 2 (DOC)</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/" class="sideboxlist"><i class="glyphicon glyphicon-duplicate"></i>Sous-dossier 3 (TXT)</a></li>
                        </ul>
                    </li>                    <li>
                        <a href="#fournisseursSubmenu" data-toggle="collapse" aria-expanded="false" class="sidebox">
                            <i class="glyphicon glyphicon-user"></i>
                            Exemple crud view id recherche update delete
                        </a>
                            <ul class="collapse list-unstyled" id="fournisseursSubmenu">
                            <li><a href="<?php echo base_url(); ?>fournisseurs/newfour" class="sideboxlist">Nouveau Fournisseur</a></li>
                            <li><a href="<?php echo base_url(); ?>fournisseurs/rechfour" class="sideboxlist">Recherche par Nom</a></li>
                        </ul>
                    </li>                                                      
                </ul>
            </nav>

