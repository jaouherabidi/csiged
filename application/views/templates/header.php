<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <link rel="icon" type="image/png" href="<?= img_url('favicon.ico') ?>" />
        <link href="<?= css_url('style') ?>" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> <!-- Appel les icones materialize -->
        <link href="<?= css_url('bootstrap') ?>" type="text/css" rel="stylesheet"   media="screen,projection"/>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>


        <title>Bienvenue - CSI GED</title>

</head>
<div id="header">
    <nav class="navbar navbar-default">
        <div class="container-fluid">

            <div class="navbar-header">
            	<br>
                <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                    <i class="glyphicon glyphicon-align-left"></i>
                    <span>Réduire</span>
                </button>
            </div>

            <div class="navbar-header">
            	<a href="<?php echo base_url(); ?>dashboard" class="box"><h1>CSI GED</h1></a>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo base_url(); ?> " class="box"><i class="glyphicon glyphicon-off"></i>  connexion / deconnexion</a></li>
                </ul>
            </div>
            <div class="nav navbar-nav navbar-right">
                <h4>Connecté en tant que : "commercial"</h4>
            </div>

        </div>
    </nav>
</div>