<?php

/**
 * This controller will manage the action for all the dashboard and it apps
 */	
class Home extends CI_Controller {
	
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect('home/upload');
    }
    
    public function dashboard(){
        $this->load->view('home/dashboard');
    }
}?>