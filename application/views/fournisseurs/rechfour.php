<?php include_once('application/views/templates/header.php'); ?>
<?php include_once('application/views/templates/sidebar.php'); ?>
<div id="content">
<div class="title-container">
Champ de recherche
</div>
	<div class="text-container">
	    <h3><i class="glyphicon glyphicon-tag"></i>   Rechercher un fournisseur</h3>
	    <hr>
       <div class="row">
      <div class="col-xs-12 col-md-5">

         <?php echo form_open("fournisseurs/search", ['class' => 'navbar-form navbar-right', 'role'=>'search']) ?>
         <p>Pour trouver le fournisseur, entrer un nom existant et valider.</p>
            <div class="form-group">
                <?php echo form_input(['name'=>'query', 'class'=>'form-control', 'placeholder' => 'Rechercher']); ?>
            </div>
            <?php echo form_submit(['value'=>'Rechercher', 'class'=>'btn btn-primary']); ?>
         <?php echo form_close(); ?>
         <?php echo form_error('query', '<div class="text-danger">', '</div>' ); ?>
      </div>
  </div>
       <h3><i class="glyphicon glyphicon-tag"></i>   Tableau de tous les fournisseurs</h3>
        <hr>
<?php if($msg = $this->session->flashdata('msg')): ?>
                		 <?php echo $msg; ?>
                	<?php endif; ?>
                	<?php echo anchor('fournisseurs/newfour', 'Ajouter un fournisseur', ['class' =>'btn btn-primary']); ?><br>
                    <br>
                   <table class="table table--striped table-hover">
                        <thead>
	                        <tr>
	                            <th>
                                    Code
	                            </th>
	                            <th>
                                    Nom
	                            </th>
	                            <th>
                                    Activité
	                            </th>
	                            <th>
                                    adresse n°1
	                            </th>
	                            <th>
                                    Ville et CP
	                            </th>                                             
	                            <th>
                                    Téléphone
	                            </th>                                            
	                            <th>
                                    Action	
	                            </th>
	                        </tr>
                        </thead>
                        <tbody>
                        	<?php if(count($posts)): ?>
                        		<?php foreach ($posts as $post): ?>
                            <tr>
                                <td>
                                    <?php echo $post->code_fournisseur; ?>
                                </td>
                                <td>
                                    <?php echo $post->nom_fournisseur; ?>
                                </td>
                                <td>
                                    <?php echo $post->activite; ?>
                                </td>
                                <td>
                                    <?php echo $post->adresse_fournisseur1; ?>
                                </td>                                
                                <td>
                                    <?php echo $post->ville_cp_fournisseur; ?>
                                </td>
                                <td>
                                    <?php echo $post->telephone; ?>
                                </td>
                                <td>
                                   <?php echo anchor("fournisseurs/view/{$post->id_fournisseur}", 'Détails', ['class' =>'label label-primary']); ?>
                                   <?php echo anchor("fournisseurs/delete/{$post->id_fournisseur}", 'Effacer', ['class' =>'label label-danger', 'id' =>'hide', 'style' => 'display:none']); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <?php else: ?>
                        		<tr>
                        			<td>Aucun enregistrement n'a été trouvé!</td>
                        		</tr>
                        <?php endif; ?>
                        </tbody>
                	</table>
                    <div class="pagination"><?php/* echo $this->pagination->create_links();  */?></div>
  	<div class="col-md-4"></div>
	</div>
</div>
</div>

<div class="clear"></div>
<?php include_once('application/views/templates/footer.php'); ?>