<?php include_once('application/views/templates/header.php'); ?>
<?php include_once('application/views/templates/sidebar.php'); ?>
<div id="content">
<div class="title-container">
	Formulaire ajout fournisseur
</div>
	<div class="text-container">


	    <h3><i class="glyphicon glyphicon-tag"></i>   Ajouter un fournisseur</h3>
	    <hr>

    <?php echo form_open('fournisseurs/save', ['class' => 'text-container']);?>
	<div class="row">
	  <div class="col-md-5">
	    <p>Nouveau fournisseur <?php echo form_input(['name' =>'nom_fournisseur', 'class' =>'form-control' ]) ;?></p>
	    <?php echo form_error('nom_fournisseur', '<div class="text-danger">', '</div>') ?>
	  </div>
	  <div class="col-md-1">
	    
	  </div>
	  <div class="col-xs-12 col-md-3">
	    <p>Code fournisseur <?php echo form_input(['name' =>'code_fournisseur', 'class' =>'form-control']) ;?></p>
	    
	  </div>
	</div>
	<div class="row">
	  <div class="col-xs-4">
	    <p>Activité <?php echo form_input(['name' =>'activite', 'class' =>'form-control' ]) ;?></p>
	    <?php echo form_error('activite', '<div class="text-danger">', '</div>') ?>
	  </div>
	  <div class="col-md-2">
	    
	  </div>
	  <div class="col-xs-6 col-md-2">
	    <p>Mode de paiement <?php 
						$options = array(
							""=>"Choisir",
							"traite" =>"Traite",
							"cheque" => "Chèque"
						);
					 ?>
        <?php echo form_dropdown('mode_paiement', $options,set_value('mode_paiement'), array("class" => "custom-select custom-select-lg mb-3", "id" => "mode_paiement")) ;?></p>
	  </div>
	</div>
	<div class="row">
	  <div class="col-xs-12 col-md-4">
	    <p>adresse <?php echo form_input(['name' =>'adresse_fournisseur1', 'class' =>'form-control' ]) ;?></p>
	    <?php echo form_error('adresse_fournisseur1', '<div class="text-danger">', '</div>') ?>
	    
	  </div>
	  <div class="col-md-2">
	    
	  </div>
	  <div class="col-md-3">
	    <p>Délai de paiement (en jours) <?php echo form_input(['name' =>'delai_paiement', 'class' =>'form-control' ]) ;?></p>
	    
	  </div>
	</div>
	<div class="row">
	  <div class="col-xs-12 col-md-4">
	    <p>Complément <?php echo form_input(['name' =>'adresse_fournisseur2', 'class' =>'form-control' ]) ;?></p>
	    
	  </div>
	  <div class="col-md-2">
	    
	  </div>
	  <div class="col-md-3">
	    <p>Mode de livraison 
	    	<?php 
						$options = array(
							""=>"Choisir",
							"routier" =>"Routier",
							"maritime" => "Maritime",
							"fluvial" => "Fluvial",
							"aerien" => "Aérien"
						);
					 ?>
        <?php echo form_dropdown('mode_livraison', $options,set_value('mode_livraison'), array("class" => "custom-select custom-select-lg mb-3", "id" => "mode_livraison")) ;?></p>
	  </div>
	</div>

	<div class="row">
	  	<div class="col-xs-12 col-md-4">
	    <p>Ville et code postal <?php echo form_input(['name' =>'ville_cp_fournisseur', 'class' =>'form-control' ]) ;?></p>
	    
	  	</div>
	  	<div class="col-md-2">
	    
	  	</div>
	  	<div class="col-md-3">
	    	<p>Délai de livraison (en jours ouvrés) <?php echo form_input(['name' =>'delai_livraison', 'class' =>'form-control' ]) ;?></p>
	  	</div>
	</div>

	<div class="row">
	  	<div class="col-xs-12 col-md-3">
	    	<p>Téléphone <?php echo form_input(['name' =>'telephone', 'class' =>'form-control' ]) ;?></p>	    
	  	</div>
	</div>
	<div class="row">
	  	<div class="col-xs-12 col-md-3">
	    	<p>Fax <?php echo form_input(['name' =>'fax', 'class' =>'form-control' ]) ;?></p>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-3">
	    	<p>Mail <?php echo form_input(['name' =>'mail', 'class' =>'form-control' ]) ;?></p>
		</div>
	</div>
                <div class="line"></div>
   <div class="row">
  	<div class="col-xs-12 col-md-4"></div>
  	<div class="col-md-4">
        <?php echo form_submit(['name' => 'submit', 'value' => 'Valider l ajout du fournisseur']); ?>
    <?php echo anchor('fournisseurs/rechfour', 'Retour', ['class' => 'btn btn-default']); ?>
	</div>
  <div class="col-md-4"></div>
</div>
<?php echo form_close(); ?>


</div>
<div class="clear"></div>
<?php include_once('application/views/templates/footer.php'); ?>