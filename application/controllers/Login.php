<?php

/**
 * This 
 */	
class Login extends CI_Controller {
	
    public function __construct()    {
        parent::__construct();
        $this->load->library('ldap');
    }

    public function index() {
        redirect('login/connexion');
    }

    public function connexion()    {
        $this->form_validation->set_rules('username', '"Identifiant"', 'trim|required|min_length[2]|max_length[52]|encode_php_tags');
        $this->form_validation->set_rules('password', '"Mot de passe"', 'trim|required|min_length[1]|max_length[52]|encode_php_tags');

        $data = array(
            'username'      => $this->input->get_post('username'),
            'password'      => $this->input->get_post('password'));

        if($this->form_validation->run() && true /*$this->Log->authentification($data['username'], $data['password'])*/){
            $ldap = new LDAP();
            $useri = new User($data['username'],$data['password']);
            $ldap->authenticate($useri);
            foreach($useri->getgroups() as $item){
                list($groupdn, $userdn, $domaindn, $suffixdn) = explode(',', $item);
                list($cn, $group) = explode('=',$groupdn);
                $profil = $this->session->set_userdata([ 'username' => $data['username'] ]);
                var_dump($group, $data);
                exit();
            }
            redirect('home/dashboard', $profil);
        }
        $this->load->view('login');
    } 
   

    public function disconnect()
    {
        $this->session->unset_userdata();
        $this->index();
    }
    
}
?>