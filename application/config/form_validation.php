<?php
	$config = array(
		'register/index' => array(
			array(
				'field' => 'user_name',
				'label' => 'identifiant',
				'rules' => 'trim|required|is_unique[user.name]',
				'errors' => array(
					'required'=> 'Veuillez entrer un %s',
					'is_unique' => 'Identifiant déjà utilisé'
				)
			),
			array(
				'field' => 'email',
				'label' => 'mail',
				'rules' => 'trim|required',
				'errors' => array(
					'required'=> 'Veuillez entrer un %s'
				)
			)
		),
		'posts/save' => array(
			array(
				'field' => 'post_title',
				'label' => 'titre',
				'rules' => 'trim|required|is_unique[tbl_posts.title]|max_length[100]',
				'errors' => array(
					'required'=> 'Veuillez entrer un %s',
					'is_unique' => 'Titre déjà utilisé',
					'max_length' => 'Trop long'
				)
			)
		),		
	);
?>