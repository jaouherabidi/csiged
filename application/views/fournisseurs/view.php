<?php include_once('application/views/templates/header.php'); ?>
<?php include_once('application/views/templates/sidebar.php'); ?>
<div id="content">
<div class="title-container">
	Page de vue fournisseur
</div>
	<div class="text-container">
	    <h3><i class="glyphicon glyphicon-tag"></i>   Fiche fournisseur</h3>
	    <hr>

	<div class="row">
	  <div class="col-md-4">
	    <p>Nom du fournisseur : <?php echo $post->nom_fournisseur; ?></p>
	  </div>
	  <div class="col-md-1">
	    
	  </div>
	  <div class="col-xs-12 col-md-3">
	    <p>Code fournisseur : <?php echo $post->code_fournisseur; ?></p>
	    
	  </div>
	</div>
	<div class="row">
	  <div class="col-xs-4">
	    <p>Activité : <?php echo $post->activite; ?></p>
	    
	  </div>
	  <div class="col-md-1">
	    
	  </div>
	  <div class="col-xs-6 col-md-4">
	    <p>Mode de paiement : <?php echo $post->mode_paiement; ?></p>
	  </div>
	</div>
	<div class="row">
	  <div class="col-xs-12 col-md-4">
	    <p>adresse : <?php echo $post->adresse_fournisseur1; ?></p>
	    
	  </div>
	  <div class="col-md-1">
	    
	  </div>
	  <div class="col-md-4">
	    <p>Délai de paiement (en jours) : <?php echo $post->delai_paiement; ?></p>
	    
	  </div>
	</div>
	<div class="row">
	  <div class="col-xs-12 col-md-4">
	    <p>Complément : <?php echo $post->adresse_fournisseur2; ?></p>
	    
	  </div>
	  <div class="col-md-1">
	    
	  </div>
	  <div class="col-md-4">
	    <p>Mode de livraison : <?php echo $post->mode_livraison; ?></p>
	  </div>
	</div>

	<div class="row">
	  	<div class="col-xs-12 col-md-4">
	    <p>Ville et code postal : <?php echo $post->ville_cp_fournisseur; ?></p>
	    
	  	</div>
	  	<div class="col-md-1">
	    
	  	</div>
	  	<div class="col-md-4">
	    	<p>Délai de livraison (en jours ouvrés) : <?php echo $post->delai_livraison; ?></p>
	  	</div>
	</div>

	<div class="row">
	  	<div class="col-xs-12 col-md-3">
	    	<p>Téléphone : <?php echo $post->telephone; ?></p>	    
	  	</div>
	</div>
	<div class="row">
	  	<div class="col-xs-12 col-md-3">
	    	<p>Fax : <?php echo $post->fax; ?></p>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-3">
	    	<p>Mail : <?php echo $post->mail; ?></p>
		</div>
	</div>
   <div class="line"></div>
   <div class="row">
  	<div class="col-xs-12 col-md-4"></div>
  	<div class="col-md-4">
    <?php echo anchor('fournisseurs/rechfour', 'Retour', ['class' => 'btn btn-default']); ?>
	</div>
  <div class="col-md-4"></div>
</div>

</div>
</div>
<div class="clear"></div>
<?php include_once('application/views/templates/footer.php'); ?>