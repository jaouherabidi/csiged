<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Forbid the user to access to all the application if he is not connected
 */
class Authorisation{
    public $ci;

    public function __construct(){
        $this->ci = &get_instance();

        $is_not_connected = null == $this->ci->session->userdata('user_profil');
        $uri_1 = $this->_checkUri('login/connexion');
        $uri_2 = $this->_checkUri('login/connexion');

        if($is_not_connected && !($uri_1 || $uri_2)){
            redirect('/login/connexion');
        }
    }

    private function _checkUri($uri){
        return 0 == strcasecmp(uri_string(), $uri);
    }

}