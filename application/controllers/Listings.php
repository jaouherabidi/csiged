<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listings extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		return redirect('listings/inventaire');
	}


	public function excel()
	{
		$this->load->model('queries');

		$queries = new Queries;
		$queries->table= 'produit';
		$queries->primary_key = 'id_produit';
		$data['produit'] = $queries->writeExcel();

		require (APPPATH .'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
		require (APPPATH .'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("");
		$objPHPExcel->getProperties()->setLastModifiedBy("");
		$objPHPExcel->getProperties()->setTitle("");
		$objPHPExcel->getProperties()->setSubject("");
		$objPHPExcel->getProperties()->setDescription("");
		
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->SetCellValue('A1','Adresse 1');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1','Adresse 2');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1','Adresse 3');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1','code-barre');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1','Référence');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1','Désignation');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1','Stock physique');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1','Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1','Quantitée inventoriée');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1','Ecart d\'inventaire');

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(22);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
 		$objPHPExcel->getActiveSheet()->getStyle('A1:N700')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$row = 2;

		foreach ($data['produit'] as $key => $value) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row,$value->ref_adresse);
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row,$value->ref_adresse2);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row,$value->ref_adresse3);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row,$value->code_barre);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row,$value->reference);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row,$value->designation);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row,$value->quantite);
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row,date("Y-m-d",strtotime($value->creation_fiche)));
			$row++;
		}

		$filename = "Fichier-inventaire-du-".date("Y-m-d-H-i-s").'.xlsx';
		$objPHPExcel->getActiveSheet()->setTitle("Listing d'inventaire Mag2LET");

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$writer->save('php://output');
		exit;

	}


	public function search_designation(){
		$this->form_validation->set_rules('query', 'Query', 'required');
		$this->form_validation->set_error_delimiters('<div class"text-danger">', '</div>');
		if($this->form_validation->run()){
			$query = $this->input->post('query');
			$this->load->model('queries');
			$post = $this->queries->searchRecord_produit_designation($query);
			$this->load->view('listings/search', ['post'=>$post]);
		}
		else{
			return redirect('listings/inventaire');
		}
	}

	public function search_reference(){
		$this->form_validation->set_rules('query2', 'Query2', 'required');
		$this->form_validation->set_error_delimiters('<div class"text-danger">', '</div>');
		if($this->form_validation->run()){
			$query2 = $this->input->post('query2');
			$this->load->model('queries');
			$post = $this->queries->searchRecord_produit_reference($query2);
			$this->load->view('listings/search', ['post'=>$post]);
		}
		else{
			return redirect('listings/inventaire');
		}
	}

	public function search_code_barre(){
		$this->form_validation->set_rules('query3', 'Query3', 'required');
		$this->form_validation->set_error_delimiters('<div class"text-danger">', '</div>');
		if($this->form_validation->run()){
			$query3 = $this->input->post('query3');
			$this->load->model('queries');
			$post = $this->queries->searchRecord_produit_code_barre($query3);
			$this->load->view('listings/search', ['post'=>$post]);
		}
		else{
			return redirect('listings/inventaire');
		}
	}

	public function search_adresse(){
		$this->form_validation->set_rules('query4', 'Query4', 'required');
		$this->form_validation->set_error_delimiters('<div class"text-danger">', '</div>');
		if($this->form_validation->run()){
			$query4 = $this->input->post('query4');
			$this->load->model('queries');
			$post = $this->queries->searchRecord_produit_adresse($query4);
			$this->load->view('listings/search', ['post'=>$post]);
		}
		else{
			return redirect('listings/inventaire');
		}
	}


	public function inventaire()
	{
		$this->load->model('queries');
		$posts = $this->queries->getProduits();
		$this->load->library('pagination');
		$config = [
			'base_url' => base_url("listings/inventaire"),
			'per_page' => 25,
			'total_rows' => $this->queries->get_num_rows_produit(),
			'uri_segment' => 3,

		];
		$this->pagination->initialize($config);
		$posts = $this->queries->getProduits($config['per_page'], $this->uri->segment(3));
		$this->load->view('listings/inventaire', ['posts' => $posts]);
	}


		public function view($id){
		$this->load->model('queries');
		$post = $this->queries->getSingleProduit($id);
		$this->load->view('listings/view', ['post' => $post]);
	}

}

