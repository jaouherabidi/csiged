<html>

     <?php $this->load->view('templates/header'); ?>
    <!--  <?php //require_once "templates/header.php"; ?>-->

    <div class="row">
        <div class="col-xs-12 col-md-4">
            <!-- Slide gauche -->
        </div>
            <!-- Zone icone -->
            <div class="col-md-1">
                <i class="toto2 light-green-text prefix">account_circle</i>

            </div>
            <!-- Page centrale -->
                <div class="col-md-3">
                <!-- Identifiant -->
                <div class="input-field">
                    <!-- <div class="test">
                    <i class="material-icons light-green-text prefix">account_circle</i> //Original
                    </div> -->
                <div class="test2">
                    <?= form_input('username', set_value('username'), 'class="toto"'); ?>
                    <?= form_label('Identifiant', 'username', 'class="active"'); ?>
                </div>
            </div>
            </div>
        <div class="col-md-4">
            <!-- Slide droite -->
        </div>
    </div>

     <div class="row">
        <div class="col-xs-12 col-md-4">
            <!-- Slide gauche -->
        </div>
            <!-- Zone icone -->
        <div class="col-md-1">
            <i class="toto2 light-green-text prefix">lock_outline</i>
        </div>
            <!-- Page centrale -->
        <div class="col-md-3">
            <!-- Mot de passe -->
            <div class="input-field">
                <!-- <div class="test">
                    <i class="material-icons light-green-text prefix">account_circle</i>
                </div> -->
                <div class="test2">
                    <?= form_password('password'); ?>
                    <?= form_label('Mot de passe', 'password', 'class="active"'); ?>
                </div>
            </div>
            </div>
        <div class="col-md-4">
            <!-- Slide droite -->
        </div>
    </div>



    <body class="">
        <div class="center section no-pad-bot">
            <div class="container">
                <div class="row col s6">
                    <?= form_open('Login/connexion'); ?>
                    <div class="card horizontal hoverable col s6 offset-l3">
                        <div class="card-stacked">
                            <div class="card-content">
                                <div class="card-title">
                                    <h5 class="header light-green-text">Connectez-vous</h5>
                                </div>
                                <div class="row">
                                    <div class="input-field">
                                        <i class="material-icons light-green-text prefix">account_circle</i>
                                        <?= form_input('username', set_value('username'), 'class="uppercase"'); ?>
                                        <?= form_label('Identifiant', 'username', 'class="active"') ?>
                                    </div>
                                    <div class="input-field">
                                        <i class="material-icons light-green-text prefix">lock_outline</i>
                                        <?= form_password('password'); ?>
                                        <?= form_label('Mot de passe', 'password', 'class="active"') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-action">
                                <div class="row">
                                    <button class="btn light-green waves-effect waves-light" type="submit" name="sub_submit">Connexion
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>

        


    </body>

    <?php $this->load->view('templates/footer.php');//Inclue le footer dans la page (avec le css ect..) ?>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?= js_url('bootstrap.min') ?>"></script>
    <script type="text/javascript" src="<?= js_url('script') ?>"></script>
</html>