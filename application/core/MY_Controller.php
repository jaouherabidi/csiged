<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	protected $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->helpers('assets_helper');
	}


	public function layout()
	{
		$this->template['header'] = $this->load->view('templates/header', $this->data, TRUE);
		$this->template['footer'] = $this->load->view('templates/footer', $this->data, TRUE);
		$this->template['sidebar'] = $this->load->view('templates/sidebar', $this->data, TRUE);
		$this->template['page'] = $this->load->view($this->page, $this->data, TRUE);
		$this->load->view('templates/main', $this->template);
	}

	public function pagin_config($num_rows= 0, $url='', $per_page=1, $uri_segment=3)
	{

		if ($uri_segment ==='')
		{
			$uri_segment = uri_string() . 'index';
		}

		$config = array(
			'base_url' => $url,
			'total_rows' => $num_rows,
			'per_page' => $per_page,
			'url_segment' => $uri_segment,
			'next_link' => 'Next',
			'prev_link' => 'Previous',
		);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		return $config;
	}

}