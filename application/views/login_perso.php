<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <link rel="icon" type="image/png" href="<?= img_url('favicon.ico') ?>" />
        <link href="<?= css_url('style') ?>" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="<?= css_url('materialize.min') ?>" type="text/css" rel="stylesheet"   media="screen,projection"/>

        <title>Bienvenue - CSI GED</title>
    </head>


    <body class="body-bg grey darken-4">
        <div class="center section no-pad-bot">
            <div class="container">
                <div class="row col s6">
                    <?= form_open('Login/connexion'); ?>
                    <div class="card horizontal hoverable col s6 offset-l3">
                        <div class="card-stacked">
                            <div class="card-content">
                                <div class="card-title">
                                    <h5 class="header light-green-text">Connectez-vous</h1>
                                </div>
                                <div class="row">
                                    <div class="input-field">
                                        <i class="material-icons light-green-text prefix">account_circle</i>
                                        <?= form_input('username', set_value('username'), 'class="uppercase"'); ?>
                                        <?= form_label('Identifiant', 'username', 'class="active"') ?>
                                    </div>
                                    <div class="input-field">
                                        <i class="material-icons light-green-text prefix">lock_outline</i>
                                        <?= form_password('password'); ?>
                                        <?= form_label('Mot de passe', 'password', 'class="active"') ?>                                     
                                    </div>
                                </div>
                            </div>
                            <div class="card-action">
                                <div class="row">
                                    <button class="btn light-green waves-effect waves-light" type="submit" name="sub_submit">Connexion
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="<?= js_url('materialize.min') ?>"></script>
        <script type="text/javascript" src="<?= js_url('script') ?>"></script>
    </body>
</html>