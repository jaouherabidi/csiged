<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<body>

<?php if ($header){ echo $header; } ?>
<?php if ($sidebar){ echo $sidebar; } ?>
<div id="content"><?php if ($page) { echo $page; } ?>

<?php if ($footer){ echo $footer; } ?>

</body>
</html>